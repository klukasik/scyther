syn region scyComment   start="/\*"  end="\*/" contains=scyComment
syn region scyLineComment start="//" end="\n"


syn region scyFunc start="send" end="_"
syn region scyFunc start="claim" end="_"
syn region scyFunc start="recv" end="_"
syn region scyFunc start="match" end="_"

syn region scyType start="symmetric-rol" end="e"

syn keyword scyTypedef protocol
syn keyword scyTypedef role
syn keyword scyTypedef usertype

syn keyword scyFunc claim
syn keyword scyFunc claim_
syn keyword scyFunc recv
syn keyword scyFunc recv_
syn keyword scyFunc send
syn keyword scyFunc send_
syn keyword scyFunc match
syn keyword scyFunc match_
syn keyword scyFunc not

syn keyword scyNumb var
syn keyword scyNumb fresh
syn keyword scyNumb const

syn keyword scyNumb Secret
syn keyword scyNumb Alive
syn keyword scyNumb Weakagree
syn keyword scyNumb Commit
syn keyword scyNumb Niagree
syn keyword scyNumb Nisynch
syn keyword scyNumb Running
syn keyword scyNumb SKR
syn keyword scyNumb Reachable
syn keyword scyNumb Empty

syn keyword scyType Nonce
syn keyword scyType Agent
syn keyword scyType Ticket
syn keyword scyType String
syn keyword scyType symmetric-role


hi link scyLineComment      Comment
hi link scyComment          Comment
hi link scyTypedef          Typedef
hi link scyType             Type
hi link scyNumb             Number
hi link scyFunc             Function
